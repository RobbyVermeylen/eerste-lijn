﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine;

public class LoadXML : MonoBehaviour
{
    MedicalAppData data = new MedicalAppData();

    public Dropdown genderDropDown;
    public Dropdown unitDropDown;
    public Dropdown packageDropDown;
    public Dropdown drawSizeDropDown;

    public InputField scenario, patient_name, medicine, tool, clipboardName;
    public Slider age;


    // CLASSES
    private Patient patient = new Patient();
    private Scenario scene = new Scenario();
    private Medicine medication = new Medicine();
    private DeliveryTool tools = new DeliveryTool();


    private void Start()
    {
        DropDown_Choices();
    }
    private void Update()
    {

    }
    public void ReadScanerio()
    {
        string filename = "";
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            filename = Application.dataPath + "/Scenario/" + Application.companyName + ".xml";
        }
        if (Application.platform == RuntimePlatform.Android)
        {
            filename = Application.persistentDataPath + Application.companyName + ".xml";
        }

        MedicalAppData.ReadFromFile(filename, out data);



        scene = data.mScenarios[0];
        patient = data.mPatients[0];
        medication = data.mMedicines[0];
        tools = data.mTools[0];

        Patient_info(patient.mName, patient.mAge, patient.mSex);


        //SCENARIO NAME

        scenario.text = scene.mName;

        //PATIENT

        patient_name.text = patient.mName;

        age.value = patient.mAge;

        genderDropDown.value = (int)patient.mSex;

        //MEDICINE 

        Medicine_info(medication.mName, medication.mClipboardName, medication.mUnit, medication.mPackage, medication.mDrawerSize);

        medicine.text = medication.mName;

        clipboardName.text = medication.mClipboardName;

        unitDropDown.value = (int)medication.mUnit;
        
        packageDropDown.value = (int)medication.mPackage;

        drawSizeDropDown.value = (int)medication.mDrawerSize;

        //TOOLS
              
        tool.text = tools.mName;
    }

    private void Patient_info(string name, int age, Sex gender)
    {
        patient.mName = name;
        patient.mAge = age;
        patient.mSex = gender;
    }
    private void Medicine_info(string medicine, string clipboardName, Unit units, Package packages, DrawerSize size)
    {
        medication.mName = medicine;
        medication.mClipboardName = clipboardName;
        medication.mUnit = units;
        medication.mPackage = packages;
        medication.mDrawerSize = size;
    }

    private void DropDown_Choices()
    {
        string[] enumGender = Enum.GetNames(typeof(Sex));
        List<string> genders = new List<string>(enumGender);

        string[] enumUnit = Enum.GetNames(typeof(Unit));
        List<string> units = new List<string>(enumUnit);

        string[] enumPackage = Enum.GetNames(typeof(Package));
        List<string> packages = new List<string>(enumPackage);

        string[] enumDrawSize = Enum.GetNames(typeof(DrawerSize));
        List<string> drawSize = new List<string>(enumDrawSize);

        genderDropDown.AddOptions(genders);
        unitDropDown.AddOptions(units);
        packageDropDown.AddOptions(packages);
        drawSizeDropDown.AddOptions(drawSize);
    }
}
