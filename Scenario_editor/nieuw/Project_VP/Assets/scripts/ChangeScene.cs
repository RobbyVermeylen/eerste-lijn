﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ChangeScene : MonoBehaviour
{
    public void SaveScene()
    {
        SceneManager.LoadScene("Save");
    }
    public void LoadScene()
    {
        SceneManager.LoadScene("Load");
    }
    public void MenuScene()
    {
        SceneManager.LoadScene("Menu");
    }
}
