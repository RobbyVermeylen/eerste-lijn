﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System;
using System.IO;


public enum DeliveryMethod
{

}
[Serializable]
public class DeliveryTool

{

    [XmlAttribute]
    public int mID;
    public string mName;

    public DeliveryTool()
    {
        mID = 0;
        mName = "";
    }
}
