﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System;
using System.IO;

[Serializable]
public enum Unit
{
    ml,
    l,
    mg,
    g,
    units
}

[Serializable]
public enum Package
{
    box,
    flask,
    bottle,
    infusion_bag,
    tablet,//single pill
    capsule,//single pill
    caplet//single pill
}

[Serializable]
public enum DrawerSize
{
    XL = 0,
    L,
    M,
    S,  
}

[Serializable]
public class Medicine {
    [XmlAttribute]
    public int mID;
    public string mName;
    public string mPrefabName;
    public string mClipboardName;
    public DrawerSize mDrawerSize;
    public int mQuantity;
    public Unit mUnit;
    public Package mPackage;
    public string mPointsOfAttention;//holds points of attention separated by # as a splitter

	public Medicine()
    {
        mName = "";
        mPrefabName = "";
        mClipboardName = "";
        mDrawerSize = DrawerSize.XL;
        mID = 0;
        mQuantity = 1;
        mUnit = Unit.ml;
        mPackage = Package.box;
        mPointsOfAttention = "";
    }

    public Medicine(string name)
    {
        mName = name;
        mPrefabName = "";
        mClipboardName = "";
        mDrawerSize = DrawerSize.XL;
        mID = 0;
        mQuantity = 0;
        mUnit = Unit.ml;
        mPackage = Package.box;
        mPointsOfAttention = "";
    }
    public Medicine(int id)
    {
        mName = "";
        mPrefabName = "";
        mClipboardName = "";
        mDrawerSize = DrawerSize.XL;
        mID = id;
        mQuantity = 0;
        mUnit = Unit.ml;
        mPackage = Package.box;
        mPointsOfAttention = "";
    }
}
