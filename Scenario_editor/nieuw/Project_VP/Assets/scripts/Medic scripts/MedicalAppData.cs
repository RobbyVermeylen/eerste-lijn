﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System;
using System.IO;

[Serializable]
public class MedicalAppData  {

    public List<Patient> mPatients;
    public List<Medicine> mMedicines;
    public List<DeliveryTool> mTools;
    public List<DeliveryMethod> mMethods;
    public List<CabinetDrawer> mDrawers;
    public List<Cabinet> mCabinets;
    public List<Scenario> mScenarios;
  //  public MetaData mMetaData;

    public MedicalAppData()
    {
       // mMetaData = new MetaData();
        mPatients = new List<Patient>() ;
        mMedicines = new List<Medicine>() ;
        mTools = new List<DeliveryTool>();
         mMethods = new List<DeliveryMethod>() ;
        mDrawers = new List<CabinetDrawer>();
        mCabinets = new List<Cabinet>();
        mScenarios = new List<Scenario>();
    }

    public static bool WriteToFile(ref MedicalAppData mediData, string filename)
    {
        bool succeeded = false;
        StreamWriter writer = null;

    
        
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MedicalAppData));
            writer = new StreamWriter(filename);
            serializer.Serialize(writer, mediData);
            succeeded = true;
        }
        catch (Exception e)
        {
            //Debug.Log("Exception writing XML: " + e.Message);
            succeeded = false;

        }
        finally
        {
            if (writer != null)
            {
                writer.Close();
            }
        }
        return succeeded;
    }

    public static bool ReadFromFile(string filename, out MedicalAppData result)
    {
        bool succeeded = false;
        result = null;
        StreamReader reader = null;
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MedicalAppData));
            reader = new StreamReader(filename);
            result = (MedicalAppData)serializer.Deserialize(reader);
            succeeded = true;
        }
        catch (Exception e)
        {
            //Debug.Log("Exception reading XML: " + e.Message);
            succeeded = false;

        }
        finally
        {
            if (reader != null)
            {
                reader.Close();
            }
        }
        return succeeded;
    }

    public string GetPatientNameFromID(int id)
    {

        for (int i = 0; i < mPatients.Count; i++)
        {
           if(mPatients[i].mID == id)
            {
                return mPatients[i].mName;
            }
        }
        return null;
    }

    public Patient GetPatientFromID(int id)
    {
        for (int i = 0; i < mPatients.Count; i++)
        {
            if (mPatients[i].mID == id)
            {
                return mPatients[i];
            }
        }
        return null;
    }

    public Medicine GetMedicineFromID(int id)
    {
        for (int i = 0; i < mMedicines.Count; i++)
        {
            if (mMedicines[i].mID == id)
            {
                return mMedicines[i];
            }
        }
        return null;
    }
    public Scenario GetScenarioFromID(int id)
    {
        for (int i = 0; i < mScenarios.Count; i++)
        {
            if (mScenarios[i].mID == id)
            {
                return mScenarios[i];
            }
        }
        return null;
    }

    public string GetMedicineNameFromID(int id)
    {

        for (int i = 0; i < mMedicines.Count; i++)
        {
            if (mMedicines[i].mID == id)
            {
                return mMedicines[i].mName;
            }
        }
        return null;
    }

    public void RemovePatientFromID(int patID)
    {
        int index = GetPatientIndex(patID);
        if (index >= 0)
        {
            mPatients.RemoveAt(index);
        }
    }
    public void RemoveMedicineFromID(int medID)
    {
        int index = GetMedicineIndex(medID);
        if (index >= 0)
        {
            mMedicines.RemoveAt(index);
        }
    }
    public void RemoveScenarioFromID(int sID)
    {
        int index = GetScenarioIndex(sID);
        if (index >= 0)
        {
            mScenarios.RemoveAt(index);
        }
    }

    public int GetPatientIndex(int patID)
    {
        for (int i = 0; i < mPatients.Count; i++)
        {
            if(mPatients[i].mID == patID)
            {
                return i;
            }
        }
        return -1;
    }
    public int GetScenarioIndex(int sID)
    {
        for (int i = 0; i < mScenarios.Count; i++)
        {
            if (mScenarios[i].mID == sID)
            {
                return i;
            }
        }
        return -1;
    }

    public int GetMedicineIndex(int medID)
    {
        for (int i = 0; i < mMedicines.Count; i++)
        {
            if (mMedicines[i].mID == medID)
            {
                return i;
            }
        }
        return -1;
    }

    public int GetNextPatientID()
    {
        int id = 1;
        foreach(Patient p in mPatients)
        {
            if(p.mID >= id)
            {
                id = p.mID + 1;
            }
        }
        return id;
    }

    public int GetNextMedicineID()
    {
        int id = 1;
        foreach (Medicine p in mMedicines)
        {
            if (p.mID >= id)
            {
                id = p.mID + 1;
            }
        }
        return id;
    }
    public int GetNextScenarioID()
    {
        int id = 1;
        foreach (Scenario s in mScenarios)
        {
            if (s.mID >= id)
            {
                id = s.mID + 1;
            }
        }
        return id;
    }

    public List<int> GetAllPatientIDs()
    {
        List<int> result = new List<int>();
        foreach (Patient p in mPatients)
        {
            result.Add(p.mID);
        }
        return result;
    }

    public List<int>GetAllMedIDs()
    {
        List<int> result = new List<int>();
        foreach (Medicine m in mMedicines)
        {
            result.Add(m.mID);
        }
        return result;
    }
}
