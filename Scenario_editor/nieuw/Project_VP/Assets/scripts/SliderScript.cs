﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SliderScript : MonoBehaviour
{
    public Slider sliderInstace;
    public Text agetext;
    // Start is called before the first frame update
    void Start()
    {
        sliderInstace.minValue = 0;
        sliderInstace.maxValue = 115;
        sliderInstace.wholeNumbers =  true;
        sliderInstace.value = 50;
    }

    // Update is called once per frame
    void Update()
    {
        agetext.text = sliderInstace.value.ToString("0");
    }
}
