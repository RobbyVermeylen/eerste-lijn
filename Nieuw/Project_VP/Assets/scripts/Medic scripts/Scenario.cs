﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System;
using System.IO;


[Serializable]
public class Scenario  {
    [XmlAttribute]
    public int mID;
    public string mName;
    public int mPatientID;
    public int mCabinetID;
    public int mMedicineID;
    public int mDeliveryMethod;
    public int mNumberOfUnitsToAdminister; //so for instance medicine xxx has a unit in Unit.ml. Then 5 means 5ml.

    public Scenario()
    {
        mID = 0;
        mName = "";
        mPatientID = 0;
        mCabinetID = 0;
        mMedicineID = 0;
        mDeliveryMethod = 0;
        mNumberOfUnitsToAdminister = 0;
    }

    public Scenario(int id)
    {
        mID = id;
        mName = "";
        mPatientID = 0;
        mCabinetID = 0;
        mMedicineID = 0;
        mDeliveryMethod = 0;
        mNumberOfUnitsToAdminister = 0;
    }



}
