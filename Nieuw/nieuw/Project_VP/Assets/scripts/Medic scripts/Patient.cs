﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System;
using System.IO;

[Serializable]
public enum PatientType
{
    adult = 0,
    child,
    pregnant,
    senior
}

public enum Sex
{
    male = 0,
    female,
    x,
   
    
}

[Serializable]
public class Patient
{
    [XmlAttribute]
    public int mID;
    public string mName;
    public int mAge;
    public int mRoom;
    public float mWeight;
    public PatientType mType;
    public Sex mSex;
    public List<string> mAllergies;

    public Patient()
    {
        mID = 0;
        mName = "John Doe";

        mAge = -1;
        mRoom = -1;
        mWeight = -100;
        mSex = Sex.male;
        mAllergies = new List<string>();

        mType = PatientType.adult;
    }

    public Patient(string name)
    {
        mID = 0;
        mName = name;

        mAge = -1;
        mRoom = -1;
        mWeight = -100;
        mSex = Sex.male;
        mAllergies = new List<string>();

        mType = PatientType.adult;
    }

    public Patient(int id, string name, int age, int room, float weight, PatientType pt, Sex s, string allergies )
    {
        mID = id;
        mName = name;

        mAge = age;
        mRoom = room;
        mWeight = weight;
        mSex = s;
        mAllergies = new List<string>();
        mAllergies.Add(allergies);

        mType = pt;
    }


}
