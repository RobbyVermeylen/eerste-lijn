﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine;

public class SaveXML : MonoBehaviour
{
    MedicalAppData data = new MedicalAppData();

    public Dropdown genderDropDown;
    public Dropdown unitDropDown;
    public Dropdown packageDropDown;
    public Dropdown drawSizeDropDown;
    
 
    

    public Text scenario, patient_name, medicine, age, tool, clipboardName;


    //CLASSES
    private Patient patient = new Patient();
    private Scenario scene = new Scenario();
    private Medicine medication = new Medicine();
    private DeliveryTool tools = new DeliveryTool();


    private void Start()
    {
        DropDown_Choices();
    }
    private void Update()
    {




    }
    public void SaveScenario()
    {
        string filename = "";
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            filename = Application.dataPath + "/Scenario/" + Application.companyName + ".xml";
        }
        if (Application.platform == RuntimePlatform.Android)
        {
            filename = Application.persistentDataPath + Application.companyName + ".xml";
        }

        //SCENARIO NAME
        scene.mName = scenario.text;
        //PATIENT NAME, AGE, GENDER
        Sex newSex = (Sex)genderDropDown.value;
        Patient_info(patient_name.text, int.Parse(age.text), newSex);


        //MEDICINE 
        //  medication = (Medicine)medicineDropDown.value ;
        Unit units = (Unit)unitDropDown.value;
        Package packages = (Package)packageDropDown.value;
        DrawerSize size = (DrawerSize)drawSizeDropDown.value;
        Medicine_info(medicine.text, clipboardName.text, units, packages, size);

        //TOOLS
        tools.mName = tool.text;
        //ADDING TO LISTS
        data.mScenarios.Add(scene);
        data.mPatients.Add(patient);
        data.mMedicines.Add(medication);
        data.mTools.Add(tools);

        MedicalAppData.WriteToFile(ref data, filename);
        // Debug.Log( MedicalAppData.WriteToFile(ref data, filename));
    }
    private void Patient_info(string name, int age, Sex gender)
    {
        patient.mName = name;
        patient.mAge = age;
        patient.mSex = gender;
    }
    private void Medicine_info(string medicine, string clipboardName, Unit units, Package packages, DrawerSize size)
    {
        medication.mName = medicine;
        medication.mClipboardName = clipboardName;
        medication.mUnit = units;
        medication.mPackage = packages;
        medication.mDrawerSize = size;
    }

  private void DropDown_Choices()
    {
        string[] enumGender = Enum.GetNames(typeof(Sex));
        List<string> genders = new List<string>(enumGender);

        string[] enumUnit = Enum.GetNames(typeof(Unit));
        List<string> units = new List<string>(enumUnit);

        string[] enumPackage = Enum.GetNames(typeof(Package));
        List<string> packages = new List<string>(enumPackage);

        string[] enumDrawSize = Enum.GetNames(typeof(DrawerSize));
        List<string> drawSize = new List<string>(enumDrawSize);

        genderDropDown.AddOptions(genders);
        unitDropDown.AddOptions(units);
        packageDropDown.AddOptions(packages);
        drawSizeDropDown.AddOptions(drawSize);
    }
    /*
    public void DropDown_Index(int index)
    {
        DropDownChoice.text = dp[index];
    }
    */
}
